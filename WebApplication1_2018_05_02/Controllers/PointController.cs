﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1_2018_05_02.Controllers
{
    public class PointController : Controller
    {
        // GET: Point
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int point)
        {

            int POINT = point;
            string level = "";
            if (POINT <20)
            {
                level = "E";
            }
            else if (20 <= POINT && POINT <=39)
            {
                level = "D";
            }
            else if (40 <= POINT && POINT <= 59)
            {
                level = "C";
            }
            else if (60 <= POINT && POINT <= 79)
            {
                level = "B";
            }
            else if (80 <= POINT && POINT <= 100)
            {
                level = "A";
            }
       
            ViewBag.POINT = POINT;
            ViewBag.level = level;
            return View();
        }

    }
}