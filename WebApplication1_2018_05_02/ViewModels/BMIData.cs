﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1_2018_05_02.ViewModels
{
    public class BMIData
    {
        [Required(ErrorMessage = "必填欄位")]
        [Range(30,150,ErrorMessage ="30-150")]
        [Display(Name ="體重")]
        public float Weight { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [Range(50, 200, ErrorMessage = "50-200")]
        [Display(Name = "身高")]
        public float Height { get; set; }

        public float BMI { get; set; }

        public string Level { get; set; }

    }
}